var PORT = 3000;

var http = require('http');
var url=require('url');
var fs=require('fs');
var path=require('path');
var Tesseract = require('tesseract.js');

var server = http.createServer(function (request, response) {
    console.log("detect image");
    response.writeHead(200, {
    'Content-Type': 'text/plain'
    });
    Tesseract.recognize("/home/ea/图片/1.png", {lang: 'eng'})
    .catch(err => console.error(err))
    .then(function(result){
        console.log("get result ",  result.text);
        response.write(result.text);
        response.end();
    })
});
server.listen(PORT);
console.log("Server runing at port: " + PORT + ".");
